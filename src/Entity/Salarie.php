<?php

namespace App\Entity;

use App\Repository\SalarieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SalarieRepository::class)
 */
class Salarie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\ManyToOne(targetEntity=Entreprise::class, inversedBy="salaries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $entreprise;

    /**
     * @ORM\ManyToOne(targetEntity=Salarie::class, inversedBy="sbires")
     */
    private $chef;

    /**
     * @ORM\OneToMany(targetEntity=Salarie::class, mappedBy="chef")
     */
    private $sbires;

    public function __construct()
    {
        $this->sbires = new ArrayCollection();
    }
    
    public function __toString(){
        return $this->getNom()." ".$this->getPrenom();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    public function setEntreprise(?Entreprise $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    public function getChef(): ?self
    {
        return $this->chef;
    }

    public function setChef(?self $chef): self
    {
        $this->chef = $chef;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getSbires(): Collection
    {
        return $this->sbires;
    }

    public function addSbire(self $sbire): self
    {
        if (!$this->sbires->contains($sbire)) {
            $this->sbires[] = $sbire;
            $sbire->setChef($this);
        }

        return $this;
    }

    public function removeSbire(self $sbire): self
    {
        if ($this->sbires->removeElement($sbire)) {
            // set the owning side to null (unless already changed)
            if ($sbire->getChef() === $this) {
                $sbire->setChef(null);
            }
        }

        return $this;
    }
}
